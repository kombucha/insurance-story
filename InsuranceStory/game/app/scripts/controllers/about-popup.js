﻿'use strict';

/**
 * @ngdoc function
 * @name gameApp.controller:AdvertisePopupCtrl
 * @description
 * # AdvertisePopupCtrl
 * Controller of the gameApp
 */
angular.module('gameApp')
  .controller('AboutPopupCtrl', function ($scope, $modalInstance) {

      $scope.ok = function () {
          $modalInstance.close();
      };

      $scope.cancel = function () {
          $modalInstance.dismiss();
      };
  });