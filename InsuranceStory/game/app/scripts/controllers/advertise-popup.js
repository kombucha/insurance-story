'use strict';

/**
 * @ngdoc function
 * @name gameApp.controller:AdvertisePopupCtrl
 * @description
 * # AdvertisePopupCtrl
 * Controller of the gameApp
 */
angular.module('gameApp').controller('AdvertisePopupCtrl', function ($scope, $modalInstance, GameService, logsService) {
    $scope.ads = [{
        title: 'Pub dans les journaux [-1000 coins]',
        cost: 1000,
        attractiveBoost: 5
    }, {
        title: 'Communication à la presse [-3000 coins]',
        cost: 3000,
        attractiveBoost: 15
    }, {
        title: 'Pub télévisée [-10000 coins]',
        cost: 10000,
        attractiveBoost: 30
    }];

    $scope.ok = function () {
        var ad = $scope.selectedAd,
            insurance = GameService.getInsurance();

        if (ad) {
            insurance.money -= ad.cost;
            insurance.contracts.forEach(function (contract) {
                contract.attractive += ad.attractiveBoost;
                contract.attractive = (contract.attractive <= 100) ? contract.attractive : 100;
            });

            logsService.add('Cette publicité vous a coûté ' + ad.cost + ', mais a renforcer l\'attractivité de vos contrats !');
        }

        $modalInstance.close($scope.contract);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss();
    };

    $scope.validOptionSelected = function () {
        var insurance = GameService.getInsurance();
        return !$scope.selectedAd || ($scope.selectedAd && $scope.selectedAd.cost < insurance.money);
    };
});
