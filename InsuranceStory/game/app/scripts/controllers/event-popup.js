﻿'use strict';

/**
 * @ngdoc function
 * @name gameApp.controller:EventPopupCtrl
 * @description
 * # EventPopupCtrl
 * Controller of the gameApp
 */
angular.module('gameApp')
  .controller('EventPopupCtrl', function ($scope, $modalInstance, options) {
      $scope.options = angular.extend({ close: "fermer", content: "", title: "" }, options);

      $scope.cancel = function () {
          $modalInstance.dismiss();
      };
});
