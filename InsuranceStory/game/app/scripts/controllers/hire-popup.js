'use strict';

/**
 * @ngdoc function
 * @name gameApp.controller:HirePopupCtrl
 * @description
 * # HirePopupCtrl
 * Controller of the gameApp
 */
angular.module('gameApp')
  .controller('HirePopupCtrl', function ($scope, $modalInstance, employeeService, insuranceService) {
      $scope.employees = employeeService.generateEmployees(Math.floor(Math.random()*3+1));
      $scope.currentPage = 1;
      $scope.ok = function () {
          insuranceService.addEmployee($scope.employees[$scope.currentPage-1]);
          $modalInstance.close();
      };

      $scope.cancel = function () {
          $modalInstance.dismiss();
      };
  });
