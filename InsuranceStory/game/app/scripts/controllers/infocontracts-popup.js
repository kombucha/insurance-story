'use strict';

/**
 * @ngdoc function
 * @name gameApp.controller:InfocontractsPopupCtrl
 * @description
 * # InfocontractsPopupCtrl
 * Controller of the gameApp
 */
angular.module('gameApp')
  .controller('InfoContractsPopupCtrl', function ($scope, $modalInstance, GameService) {
      $scope.currentPage = 1;
      $scope.contracts = GameService.getInsurance().contracts;
      $scope.ok = function () {
          $modalInstance.close();
      };

      $scope.cancel = function () {
          $modalInstance.dismiss();
      };
  });
