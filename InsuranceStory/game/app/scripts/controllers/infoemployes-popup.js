'use strict';

/**
 * @ngdoc function
 * @name gameApp.controller:InfoemployesPopupCtrl
 * @description
 * # InfoemployesPopupCtrl
 * Controller of the gameApp
 */
angular.module('gameApp')
  .controller('InfoEmployesPopupCtrl', function ($scope, $modalInstance, GameService) {
      $scope.employees = GameService.getInsurance().employees;
    $scope.currentPage = 1;
      $scope.ok = function () {
          $modalInstance.close();
      };

      $scope.cancel = function () {
          $modalInstance.dismiss();
      };
  });
