'use strict';

/**
 * @ngdoc function
 * @name gameApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the gameApp
 */
angular.module('gameApp').controller('MainCtrl', function ($scope, GameService, insuranceService, logsService) {
    $scope.insurance = insuranceService.createInsurance();
    $scope.debug = $scope.insurance;

    $scope.pause = function() {
        GameService.pause();
    };
    $scope.resume = function () {
        GameService.resume();
    };
    // Start Game !
    GameService.start($scope.insurance);

    $scope.logs = logsService.get;

    $scope.insuranceType = function() {
        var value = $scope.insurance.satisfaction;
        if (value < 25) {
            return 'danger';
        } else if (value < 50) {
            return 'warning';
        } else if (value < 75) {
            return 'info';
        } else {
            return 'success';
        }
    };
});
