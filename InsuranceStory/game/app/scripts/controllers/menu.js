﻿'use strict';

/**
 * @ngdoc function
 * @name gameApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the gameApp
 */
angular.module('gameApp').controller('MenuCtrl', function ($scope, $modal, GameService, $q) {
    $scope.newContract = function () {
        openModal('newcontract-popup', 'NewcontractPopupCtrl');
    };
    $scope.advertise = function () {
        openModal('advertise-popup', 'AdvertisePopupCtrl');

    };
    $scope.train = function () {
        openModal('train-popup', 'TrainPopupCtrl');

    };
    $scope.hire = function () {
        openModal('hire-popup', 'HirePopupCtrl');

    };
    $scope.infoContrats = function () {
        openModal('infocontracts-popup', 'InfoContractsPopupCtrl');
    };
    $scope.infoEmployes = function () {
        openModal('infoemployes-popup', 'InfoEmployesPopupCtrl');
    };

    $scope.about = function () {
        openModal('about-popup', 'AboutPopupCtrl');
    };

    function openModal (template, controller) {
        GameService.pause();
        var defer = $q.defer();
        $modal.open({
            templateUrl: './views/'+template+'.html?t=' + Date.now(),
            controller: controller,
        }).result.then(function(result) {
            GameService.resume();
            defer.resolve(result);
        }, function(result) {
            GameService.resume();
            defer.reject(result);
        });

        return defer.promise;
    }
});
