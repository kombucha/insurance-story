﻿'use strict';

/**
 * @ngdoc function
 * @name gameApp.controller:NewcontractPopupCtrl
 * @description
 * # NewcontractPopupCtrl
 * Controller of the gameApp
 */
angular.module('gameApp')
  .controller('NewcontractPopupCtrl', function ($scope, $modalInstance, contractService, contractModel, needsConstant, targetsConstant) {
      $scope.needs = needsConstant;
      $scope.targets = targetsConstant;
      $scope.contract = new contractModel();

      $scope.ok = function () {
          contractService.createContract($scope.contract.name, $scope.contract.need, $scope.contract.target);
          $modalInstance.close();
      };

      $scope.cancel = function () {
          $modalInstance.dismiss();
      };
});
