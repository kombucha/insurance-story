'use strict';

/**
 * @ngdoc function
 * @name gameApp.controller:TrainPopupCtrl
 * @description
 * # TrainPopupCtrl
 * Controller of the gameApp
 */
angular.module('gameApp')
    .controller('TrainPopupCtrl', function($scope, $modalInstance, GameService) {
        $scope.employees = GameService.getInsurance().employees;
        $scope.currentPage = 1;
        $scope.ok = function() {
            $modalInstance.close();
        };

        $scope.addPoint = function(competence) {
            var employee = $scope.employees[$scope.currentPage - 1];
            if (employee.competencePoints <= 0)
                return;

            employee.competencePoints--;
            employee[competence]++;

        }

        $scope.cancel = function() {
            $modalInstance.dismiss();
        };
    });