'use strict';

/**
 * @ngdoc directive
 * @name gameApp.directive:gameTimer
 * @description
 * # gameTimer
 */
angular.module('gameApp').directive('gameTimer', function () {
    return {
        templateUrl: 'views/gametimer.html',
        restrict: 'E',
        scope: {
            timeInWeeks: '='
        },
        link: function postLink(scope) {
            scope.$watch('timeInWeeks', function (timeInWeeks) {
                scope.years = Math.floor(timeInWeeks / 48) + 1;
                scope.months = (Math.floor(timeInWeeks / 4) % 12) + 1;
                scope.weeks = (timeInWeeks % 4) + 1;
            });
        }
    };
});
