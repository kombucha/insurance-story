'use strict';

/**
 * @ngdoc filter
 * @name gameApp.filter:formattedDate
 * @function
 * @description
 * # formattedDate
 * Filter in the gameApp.
 */
angular.module('gameApp').filter('formattedGameTime', function () {
    return function (totalWeeks) {
        var numberOfYears = Math.floor(totalWeeks / 48) + 1,
            numberOfMonths = (Math.floor(totalWeeks / 4) % 12) + 1,
            numberOfWeeks = (totalWeeks % 4) + 1;

        return 'Y' + numberOfYears + ' M' + numberOfMonths + ' W' + numberOfWeeks;
    };
});
