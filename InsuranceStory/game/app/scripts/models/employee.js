﻿angular.module('gameApp').service('employeeModel',[function() {
    return function () {
        this.name = 'Jean Dupont ' + Math.random();
        this.salary = 100;
        this.role = undefined;
        this.description = undefined;

        // Qualities
        this.speed = 1;
        this.expertise = 1;
        this.clientRelation = 1;
        this.creativity = 1;
        this.level = 1;
        this.competencePoints = 1;
    };
}]);
