﻿angular.module('gameApp').service('InsuranceModel', [function () {
    return function () {
        this.name = '';
        this.age = 0;

        this.contracts = [];
        this.employees = [];

        this.clients = 0;
        this.satisfaction = 25;
        this.money = 5000;
    };
}]);
