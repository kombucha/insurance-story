﻿angular.module('gameApp').constant('roleEnum', {
    ChargeClientele: 0,
    Regleur: 1,
    Expert: 2,
    Actuaire: 3,
    Publicitaire: 4
});