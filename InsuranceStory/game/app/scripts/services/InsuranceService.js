﻿'use strict';

angular.module('gameApp').factory('insuranceService', function (InsuranceModel, roleEnum, employeeService, logsService) {
    var insuranceService = {};
    var insurance = {};
    insuranceService.createInsurance = function () {
        insurance = new InsuranceModel();
        insurance.employees.push(employeeService.createEmployee(roleEnum.ChargeClientele));
        insurance.employees.push(employeeService.createEmployee(roleEnum.Actuaire));

        return insurance;
    };

    insuranceService.addContract =function(contract) {
        insurance.contracts.push(contract);
        logsService.add("Vous venez de sortir le contrat " + contract.name + " !");
    };

    insuranceService.getEmployees = function() {
        return insurance.employees;
    };

    insuranceService.addEmployee = function (employee) {
        logsService.add("Vous venez d'embaucher " + employee.name + " !");
        return insurance.employees.push(employee);
    };

    insuranceService.deleteContracts = function(deletedContracts) {
        deletedContracts.forEach(function(contract) {
            var index = insurance.contracts.indexOf(contract);
            if (index > -1) {
                insurance.contracts.splice(index, 1);
            }
        });
    };

    return insuranceService;

});