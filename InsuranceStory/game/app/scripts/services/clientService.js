﻿'use strict';


angular.module('gameApp').factory('clientService', function (clientModel) {
    var clientService = {};

    clientService.generateClient = function() {
        var client = new clientModel();
        return client;
    };

    return clientService;

});