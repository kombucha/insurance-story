﻿'use strict';

angular.module('gameApp').factory('contractService', function (contractModel, insuranceService, toolsService) {
    var contractService = {};

    var calculPrice = function (need, cible) {
        var basePrice = 10;
        switch (need.id) {
            case 1:
                basePrice = 20;
                break;
            case 2:
                basePrice = 10;
                break;
            case 3:
                basePrice = 25;
                break;
            case 4:
                basePrice = 5;
                break;
        }

        switch (cible.id) {
            case 1:
                basePrice *= 2;
                break;
            case 2:
                basePrice *= 0.75;
                break;
            case 3:
            case 4:
                basePrice *= 1;
                break;
            case 5:
                basePrice *= 5;
                break;
        }

        var employees = insuranceService.getEmployees();
        return basePrice * employees.length;
    };

    var calculIndemnity = function (need, cible) {
        var basePrice = 100;
        switch (need.id) {
            case 1:
                basePrice = 200;
                break;
            case 2:
                basePrice = 50;
                break;
            case 3:
                basePrice = 100;
                break;
            case 4:
                basePrice = 25;
                break;
        }

        switch (cible.id) {
            case 1:
                basePrice *= 2;
                break;
            case 2:
                basePrice *= 0.75;
                break;
            case 3:
            case 4:
                basePrice *= 1;
                break;
            case 5:
                basePrice *=1;
                break;
        }


        var employees = insuranceService.getEmployees();

        return basePrice / employees.length;
    };

    var calculAttractive = function (need) {
        var base = toolsService.getRandomizer(0, 10);
        var ratio = 0;
        insuranceService.getEmployees().forEach(function(employee) {
            if (employee.role == 'Publicitaire')
                ratio += employee.creativity;
        });

        if (ratio > 0) {
           base = base + (ratio / 3);
        }

        return base;
    };

    contractService.createContract = function (name,need,cible) {
        var contract = new contractModel();
        contract.name = name;
        contract.price = calculPrice(need, cible);
        contract.target = cible;
        contract.need = need;
        contract.indemnity = calculIndemnity(need, cible);
        // week
        contract.delay = toolsService.getRandomizer(4, 6)*48;
        contract.attractive = calculAttractive(need);

        insuranceService.addContract(contract);
        return contract;
    };

    return contractService;

});