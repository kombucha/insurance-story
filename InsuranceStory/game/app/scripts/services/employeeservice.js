﻿'use strict';

angular.module('gameApp').factory('employeeService', function (employeeModel, toolsService, roleEnum) {
    var employeeService = {};

    employeeService.createEmployee = function (role) {
        var employee = new employeeModel();
        employee.name = toolsService.getName();
        switch (role) {
            case roleEnum.ChargeClientele:
                employee.role = 'Chargé de clientèle';
                employee.speed = 2;
                employee.expertise = 2;
                employee.clientRelation = 4;
                employee.creativity = 1;
                employee.salary = toolsService.getRandomizer(100, 200);
                employee.description = "Il fait de la prospection commerciale.<br />Il démarche tout type de clients (particulier, pro) pour développer et enrichir son portefeuille.";
                break;
            case roleEnum.Regleur:
                employee.role = 'Régleur';
                employee.speed = 3;
                employee.expertise = 1;
                employee.clientRelation = 1;
                employee.creativity = 1;
                employee.salary = toolsService.getRandomizer(100, 200);
                employee.description = "Interlocuteur des victimes.<br />Il analyse les sinistres et indemnise les victimes.";
                break;
            case roleEnum.Expert:
                employee.role = 'Expert assurance';
                employee.speed = 3;
                employee.expertise = 1;
                employee.clientRelation = 1;
                employee.creativity = 1;
                employee.salary = toolsService.getRandomizer(100, 200);
                employee.description = "Il écrit un rapport afin de décrire le sinistre en détail.<br />Quand, quoi, comment, pour quel coût sont les question auquels il doit répondre.";
                break;
            case roleEnum.Actuaire:
                employee.role = 'Actuaire';
                employee.speed = 3;
                employee.expertise = 1;
                employee.clientRelation = 4;
                employee.creativity = 1;
                employee.salary = toolsService.getRandomizer(100, 200);
                employee.description = "Mathématicien qui fait des calculs statistiques sur les assurances.<br />Il gère les conséquences financières qui découlent d'évenements incertains.";
                break;
            case roleEnum.Publicitaire:
            default :
                employee.role = 'Publicitaire';
                employee.speed = 3;
                employee.expertise = 1;
                employee.clientRelation = 1;
                employee.creativity = 3;
                employee.salary = toolsService.getRandomizer(100, 200);
                employee.description = "Il conçoit les campagnes marketing qui vont vous permettre de vendre vos produits.";
                break;
        }

        return employee;
    };

    employeeService.generateEmployees = function(number) {
        var employees = [];
        for (var i = 0; i < number; i++) {
            employees.push(employeeService.createEmployee(toolsService.getRandomizer(0, 4)));
        }

        return employees;
    };


    return employeeService;

});