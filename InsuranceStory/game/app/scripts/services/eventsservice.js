﻿'use strict';

angular.module('gameApp').factory('eventsService', function ($modal, $q) {
    var eventsService = {};
    eventsService.add = function (text, title, closeButton) {
        var defer = $q.defer();
        $modal.open({
            templateUrl: './views/event-popup.html?t=' + Date.now(),
            controller: 'EventPopupCtrl',
            resolve: {
                options: function () {
                    return { content: text, title: title, close: closeButton };
                }
            }
        }).result.then(function (result) {
            defer.resolve(result);
        }, function (result) {
            defer.reject(result);
        });
        return defer.promise;
    };
    return eventsService;
});