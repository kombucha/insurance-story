'use strict';

/**
 * @ngdoc service
 * @name gameApp.GameDisplay
 * @description
 * # GameDisplay
 * Factory in the gameApp.
 */
angular.module('gameApp').factory('GameDisplay', function ($window, $q, toolsService) {
    var Phaser = $window.Phaser,
        gameStartDefer = $q.defer(),
        game,
        insurance,
        api = {};

    function getBootState() {
        return {
            preload: function () {
                this.game.load.image('progressBar', 'images/progressBar.png');
            },

            create: function () {
                this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

                this.game.stage.backgroundColor = '#0D529C';
                this.game.physics.startSystem(Phaser.Physics.ARCADE);

                this.game.state.start('load');
            },
        };
    }

    function getLoadState() {
        return {
            preload: function () {
                // Loading text
                var loadingLabel = this.game.add.text(this.game.world.centerX, 150,
                    'loading...', {
                        font: '30px Geo',
                        fill: '#fff'
                    });

                loadingLabel.anchor.setTo(0.5, 0.5);

                // Progress bar
                var progressBar = this.game.add.sprite(this.game.world.centerX, 200, 'progressBar');
                progressBar.anchor.setTo(0.5, 0.5);
                this.game.load.setPreloadSprite(progressBar);

                // Load all assets
                this.game.load.image('background', 'images/background.png');
                this.game.load.image('plant', 'images/plant.png');
                this.game.load.image('desk', 'images/desk.png');
                this.game.load.image('employee', 'images/guy-in-chair.png');
                this.game.load.image('couple', 'images/couple.png');
                this.game.load.image('guy', 'images/guy.png');
                this.game.load.image('chick', 'images/chick.png');
                this.game.load.image('chair', 'images/chair.png');
                this.game.load.image('happy', 'images/bubble-happy.png');
                this.game.load.image('sad', 'images/bubble-unhappy.png');
            },

            create: function () {
                this.game.state.start('play');
            }
        };
    }

    function getPlayState() {
        return {
            create: function () {
                gameStartDefer.resolve();
                this.createGraphics();
                this.launchClientAnimations();
            },

            createGraphics: function () {
                // this.game.stage.smoothed = false;
                this.game.add.sprite(0, 0, 'background');

                // Client
                this.clientsGroup = this.game.add.group();
                this.clientsGroup.createMultiple(1, 'guy');
                this.clientsGroup.createMultiple(1, 'chick');
                this.clientsGroup.createMultiple(1, 'couple');

                // Employees
                this.deskGroup = this.game.add.group();
                this.employeesGroup = this.game.add.group();
                this.chairGroup = this.game.add.group();
                var i, x, y;

                for (i = 0; i < 6; i++) {
                    x = (i % 2 !== 0) ? (this.game.width - 120) : 50;
                    y = 130 + Math.floor(i / 2) * 100;

                    this.deskGroup.create(x, y, 'desk');
                    this.employeesGroup.create(x + 35, y, 'employee');
                    this.chairGroup.create(x + 25, y + 15, 'chair');
                }

                // Decoration
                this.game.add.sprite(this.game.width - 70, 45, 'plant');
            },

            launchClientAnimations: function () {
                game.time.events.loop(5 / Phaser.Timer.SECOND, this.spawnClient, this);
            },

            spawnClient: function () {
                var client = this.clientsGroup.getFirstDead(),
                    availableDeskIdx = this.getAvailableDesk(),
                    bubble;

                if (!client || availableDeskIdx === -1) {
                    return;
                }

                var deskPosition = this.computeClientPositionFromDesk(availableDeskIdx),
                    timeAtDesk = toolsService.getRandomizer(2000, 5000),
                    bubbleSprite = !!toolsService.getRandomizer(0, 1) ? 'happy' : 'sad';

                client.reset(deskPosition.x, deskPosition.y);
                client.alpha = 1;

                this.game.add.tween(client)
                    .to({
                        alpha: 0
                    }, 1000, Phaser.Easing.Linear.None, true, timeAtDesk)
                    .start()
                    .onComplete.addOnce(function () {
                        client.kill();
                    });

                // FIXME: Spawn bubble everytime, NOT efficient
                var emote = this.game.add.sprite(deskPosition.x + 25, deskPosition.y - 20, bubbleSprite);
                emote.alpha = 0;
                this.game.add.tween(emote).to({
                        alpha: 1
                    }, 1000, Phaser.Easing.Linear.None, true, timeAtDesk - 1000)
                    .start()
                    .onComplete.addOnce(function () {
                        emote.kill();
                    });
            },

            getAvailableDesk: function () {
                var employeesCount = insurance.employees.length;
                return toolsService.getRandomizer(0, employeesCount - 1);
            },

            occupyDesk: function (index) {
                // TODO
            },

            freeDesk: function (index) {
                // TODO
            },

            computeClientPositionFromDesk: function (deskIndex) {
                var x = (deskIndex % 2 !== 0) ? (this.game.width - 120) : 50,
                    y = 130 + Math.floor(deskIndex / 2) * 100;

                return new Phaser.Point(x + 25, y - 40);
            },

            update: function () {
                this.handlePhysics();
                this.handleInput();

                this.updateEmployees();
            },

            updateEmployees: function () {
                var employeesCount = insurance.employees.length;
                this.employeesGroup.setAll('visible', false);
                this.employeesGroup.forEach(function (employee) {
                    if (employeesCount-- > 0) {
                        employee.visible = true;
                    }
                });
            },

            handlePhysics: function () {},

            handleInput: function () {}
        };
    }

    api.init = function (insuranceParam) {
        insurance = insuranceParam;

        if (game) {
            return gameStartDefer.promise;
        }

        game = new Phaser.Game(320,  420, Phaser.AUTO, 'phaser-container');

        game.state.add('boot', getBootState());
        game.state.add('load', getLoadState());
        game.state.add('play', getPlayState());

        game.state.start('boot');

        return gameStartDefer.promise;
    };

    return api;
});
