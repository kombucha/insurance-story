﻿'use strict';

/**
 * @ngdoc service
 * @name gameApp.gameService
 * @description
 * # gameService
 * Factory in the gameApp.
 */
angular.module('gameApp').factory('GameService', function ($window, $rootScope, GameDisplay, toolsService, insuranceService, eventsService, logsService) {
    var api = {},
        isRunning = false,
        updateInterval,
        // in ms
        UPDATE_DELAY = 1000,
        insurance;

    // Update loop
    function onTick() {
        if (!isRunning) {
            return;
        }

        updateTime();

        // Stuff that updates every week
        // TODO

        // Stuff that updates only on a new month
        if ((insurance.age % 4) === 1) {
            updateMoney();
            updateContracts();
            updateClientsNumber();
        }

        if (!$rootScope.$$phase) {
            $rootScope.$apply();
        }
    }

    function updateTime() {
        insurance.age++;
    }

    function getSinisterNumber(contract) {
        if (contract.clients > 10)
            return toolsService.getRandomizer(0, 10);

        return toolsService.getRandomizer(0, contract.clients);
        //switch(contract.)


    }

    function updateMoney() {
        // Remove employee salaries
        insurance.employees.forEach(function (employee) {
            insurance.money -= employee.salary;
        });


        var sinisterCount = 0;
        insurance.contracts.forEach(function (contract) {
            // Add contracts money
            insurance.money += contract.clients * contract.price;

            // Remove sinisters
            var sinisters = getSinisterNumber(contract);
            sinisterCount += sinisters;
            insurance.money -= sinisters * contract.indemnity;

            var satisfaction = (contract.clients - sinisters) / contract.clients;

            if (satisfaction == 0) {
                insurance.satisfaction -= 5;
            } else {
                if (satisfaction < 0.25) {
                    insurance.satisfaction -= 1;
                }
                else {
                    if (satisfaction < 0.5) {
                        insurance.satisfaction += 0;
                    } else {
                        if (satisfaction < 0.75) {
                            insurance.satisfaction += 1;
                        }
                        else {
                            if (satisfaction <= 1) {
                                insurance.satisfaction += 3;
                            }
                        }
                    }
                }
            }
            if (insurance.satisfaction < 0)
                insurance.satisfaction = 0;
            if (insurance.satisfaction > 100)
                insurance.satisfaction = 100;

        });
if(sinisterCount > 0)
        logsService.add(sinisterCount + " sinistres cette semaine!");
    }

    function updateContracts() {
        var deletedContracts = [];

        insurance.contracts.forEach(function (contract) {
            contract.delay--;
            if (contract.delay <= 0) {
                deletedContracts.push(contract);
            }
        });

        if (deletedContracts.length > 0) {
            insuranceService.deleteContracts(deletedContracts);
        }
    }

    function getClientNumber(contract) {
        var ratio = 0;
        insurance.employees.forEach(function (employee) {
            if (employee.role == 'Chargé de clientèle')
                ratio += employee.clientRelation;

            if (employee.role == 'Publicitaire')
                ratio += employee.creativity;
        });
        ratio = ratio % 5;
        return toolsService.getRandomizer(0 + ratio, contract.attractive + ratio);
    }

    function updateClientsNumber() {
        insurance.clients = 0;
        var newClients = 0;
        insurance.contracts.forEach(function (contract) {
            var newClient = getClientNumber(contract);
            newClients += newClient;
            contract.clients += newClient;
            insurance.clients += contract.clients;
        });
        logsService.add(newClients + " nouveaux clients cette semaine!");
    }


    function interval(callback, delay) {
        delay = delay || 100;
        var startTime = Date.now(),
            shouldCancel = false,
            currentTime,
            _callback = function () {
                if (Date.now() - startTime > delay) {
                    callback();
                    startTime = Date.now();
                }

                if (!shouldCancel) {
                    $window.requestAnimationFrame(_callback);
                }
            };

        $window.requestAnimationFrame(_callback);

        return {
            cancel: function () {
                shouldCancel = true;
            }
        }
    }

    api.getInsurance = function () {
        return insurance;
    };

    // Game control
    api.start = function (insuranceParam) {
        insurance = insuranceParam;

        GameDisplay.init(insurance);
        updateInterval = interval(onTick, UPDATE_DELAY);
        logsService.add("Bienvenue dans Insurance Story!");
        eventsService.add("Bienvenue dans Insurance Story! Dans ce jeu, découvrez le métier d'assureur en créant vos propres contrats, puis assurez les personnes et gérez leurs sinistres.<br/><br/>Bon courage!", "Insurance Story","J'ai compris").then(function() {
            isRunning = true;
        },function() { isRunning = true; });
    };

    api.pause = function () {
        isRunning = false;
    };

    api.resume = function () {
        isRunning = true;
    };

    return api;
});
