﻿'use strict';

/**
 * @ngdoc service
 * @name gameApp.jobsconstant
 * @description
 * # ciblesConstant
 * Constant in the gameApp.
 */
angular.module('gameApp')
  .constant('jobsConstant', [
      { id: 1, name: "Chargé de clientèle" },
      { id: 2, name: "Régleur" },
      { id: 3, name: "Expert" },
      { id: 4, name: "Actuaire" },
      { id: 5, name: "Publicitaire" }
  ]);
