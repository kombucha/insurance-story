﻿'use strict';


angular.module('gameApp').factory('logsService', function () {
    var logs = [];
    var logsService = {};
    logsService.add = function (text) {
        return logs.splice(0,0,text);
    };

    logsService.get = function () {
        return logs;
    };

    return logsService;
});