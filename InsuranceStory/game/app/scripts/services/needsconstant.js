﻿'use strict';

/**
 * @ngdoc service
 * @name gameApp.besoinsConstant
 * @description
 * # besoinsConstant
 * Constant in the gameApp.
 */
angular.module('gameApp')
  .constant('needsConstant', [
      { id: 1, name: "Auto" },
      { id: 2, name: "Habitation" },
      { id: 3, name: "Vie" },
      { id: 4, name: "Santé" }
  ]);
