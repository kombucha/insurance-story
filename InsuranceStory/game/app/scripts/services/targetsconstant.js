﻿'use strict';

/**
 * @ngdoc service
 * @name gameApp.ciblesConstant
 * @description
 * # ciblesConstant
 * Constant in the gameApp.
 */
angular.module('gameApp')
  .constant('targetsConstant', [
      { id: 1, name: "Jeune" },
      { id: 2, name: "Famille" },
      { id: 3, name: "Entreprise" },
      { id: 4, name: "Sportif" },
      { id: 5, name: "Personne agé" }
  ]);
