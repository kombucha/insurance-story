﻿'use strict';


angular.module('gameApp').factory('toolsService', function () {
    var toolsService = {};

    var firstname = ['Jérôme', 'Vincent', 'Pierre', 'Moussa', 'John', 'Paul', 'Jacques'];
    var lastname = ['Dupond', 'Dupont', 'Dupuis', 'Doe'];

    toolsService.getRandomizer = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };

    toolsService.getName = function() {
        return firstname[toolsService.getRandomizer(0, firstname.length - 1)] + ' '
            + lastname[toolsService.getRandomizer(0, lastname.length - 1)];
    };

    return toolsService;
});