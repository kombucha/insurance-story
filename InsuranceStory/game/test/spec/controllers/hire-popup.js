'use strict';

describe('Controller: HirePopupCtrl', function () {

  // load the controller's module
  beforeEach(module('gameApp'));

  var HirePopupCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    HirePopupCtrl = $controller('HirePopupCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
