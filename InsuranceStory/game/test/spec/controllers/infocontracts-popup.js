'use strict';

describe('Controller: InfocontractsPopupCtrl', function () {

  // load the controller's module
  beforeEach(module('gameApp'));

  var InfocontractsPopupCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    InfocontractsPopupCtrl = $controller('InfocontractsPopupCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
