'use strict';

describe('Controller: InfoemployesPopupCtrl', function () {

  // load the controller's module
  beforeEach(module('gameApp'));

  var InfoemployesPopupCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    InfoemployesPopupCtrl = $controller('InfoemployesPopupCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
