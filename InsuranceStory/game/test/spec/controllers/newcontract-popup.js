'use strict';

describe('Controller: NewcontractPopupCtrl', function () {

  // load the controller's module
  beforeEach(module('gameApp'));

  var NewcontractPopupCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    NewcontractPopupCtrl = $controller('NewcontractPopupCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
