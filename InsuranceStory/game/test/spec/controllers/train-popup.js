'use strict';

describe('Controller: TrainPopupCtrl', function () {

  // load the controller's module
  beforeEach(module('gameApp'));

  var TrainPopupCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TrainPopupCtrl = $controller('TrainPopupCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
