'use strict';

describe('Service: besoins', function () {

  // load the service's module
  beforeEach(module('gameApp'));

  // instantiate service
  var besoins;
  beforeEach(inject(function (_besoins_) {
    besoins = _besoins_;
  }));

  it('should do something', function () {
    expect(!!besoins).toBe(true);
  });

});
