'use strict';

describe('Service: besoinsConstant', function () {

  // load the service's module
  beforeEach(module('gameApp'));

  // instantiate service
  var besoinsConstant;
  beforeEach(inject(function (_besoinsConstant_) {
    besoinsConstant = _besoinsConstant_;
  }));

  it('should do something', function () {
    expect(!!besoinsConstant).toBe(true);
  });

});
