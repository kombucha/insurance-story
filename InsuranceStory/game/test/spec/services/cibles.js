'use strict';

describe('Service: cibles', function () {

  // load the service's module
  beforeEach(module('gameApp'));

  // instantiate service
  var cibles;
  beforeEach(inject(function (_cibles_) {
    cibles = _cibles_;
  }));

  it('should do something', function () {
    expect(!!cibles).toBe(true);
  });

});
