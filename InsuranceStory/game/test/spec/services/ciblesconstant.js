'use strict';

describe('Service: ciblesConstant', function () {

  // load the service's module
  beforeEach(module('gameApp'));

  // instantiate service
  var ciblesConstant;
  beforeEach(inject(function (_ciblesConstant_) {
    ciblesConstant = _ciblesConstant_;
  }));

  it('should do something', function () {
    expect(!!ciblesConstant).toBe(true);
  });

});
