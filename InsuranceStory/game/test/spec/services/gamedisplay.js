'use strict';

describe('Service: GameDisplay', function () {

  // load the service's module
  beforeEach(module('gameApp'));

  // instantiate service
  var GameDisplay;
  beforeEach(inject(function (_GameDisplay_) {
    GameDisplay = _GameDisplay_;
  }));

  it('should do something', function () {
    expect(!!GameDisplay).toBe(true);
  });

});
