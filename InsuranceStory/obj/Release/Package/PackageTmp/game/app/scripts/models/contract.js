﻿angular.module('gameApp').service('contractModel', [function () {
    return function() {
        this.name = 'Contrat ' + Math.random();
        this.price = 1;
        this.cible = undefined;
        this.need = undefined;
        this.indemnity = 0;
        // week
        this.delay = 1;
        this.attractive = Math.random() * 100;
        this.clients = 0;
    };
}]);
