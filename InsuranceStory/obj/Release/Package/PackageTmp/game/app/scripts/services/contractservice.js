﻿'use strict';

angular.module('gameApp').factory('contractService', function (contractModel, insuranceService, toolsService) {
    var contractService = {};

    var calculPrice=function(need,cible) {
        var employees = insuranceService.getEmployees();
        return 10;
    };

    var calculIndemnity = function (need, cible) {
        var employees = insuranceService.getEmployees();
        return 100;
    };


    contractService.createContract = function (name,need,cible) {
        var contract = new contractModel();
        contract.name = name;
        contract.price = calculPrice(need, cible);
        contract.cible = cible;
        contract.need = need;
        contract.indemnity = calculIndemnity(need, cible);
        // week
        contract.delay = toolsService.getRandomizer(4, 6)*48;


        insuranceService.addContract(contract);
        return contract;
    };

    return contractService;

});